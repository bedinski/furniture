#pragma once
#include "Material.h"

class Wood : public Material
{
public:
	Wood()
	{
		name = "wood";
		fireproof = false;
	}
};

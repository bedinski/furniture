#pragma once
#include "FurnitureComponent.h"

class Top : public FurnitureComponent
{
public:
	Top(Material mat)
	{
		material = mat;
	}

	bool FurnitureComponent::isDurable() const override
	{
		return material.isFireproof();
	}

	~Top()
	{
		std::cout << "Top is destroyed" << std::endl;
	}
};

#pragma once
#include <string>

class Material
{
protected:
	std::string name;
	bool fireproof;

public:

	virtual std::string getName()
	{
		return name;
	}

	virtual bool isFireproof() const
	{
		return fireproof;
	}
};

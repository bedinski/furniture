#pragma once
#include "FurnitureComponent.h"

class Leg : public FurnitureComponent
{
public:
	Leg(Material mat)
	{
		material = mat;
	}

	bool FurnitureComponent::isDurable() const override
	{
		return material.isFireproof();
	}

	~Leg()
	{
		std::cout << "Leg is destroyed" << std::endl;
	}
};

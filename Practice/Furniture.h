#pragma once

#include "FurnitureComponent.h"
#include <vector>

class Furniture
{
private:
	std::vector<std::shared_ptr<FurnitureComponent>> components;
public:
	void addComponent(std::shared_ptr<FurnitureComponent> const &component);

	bool isDurable() const;
};

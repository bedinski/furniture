#pragma once
#include "Material.h"
#include <iostream>

class FurnitureComponent
{
protected:
	Material material;

public :
	// default constructor
	FurnitureComponent() = default;

	// copy constructor
	FurnitureComponent(const FurnitureComponent& copy) = default;

	// copy assignment operator
	FurnitureComponent& operator=(const FurnitureComponent& copy) = default;

	// move constructor
	FurnitureComponent(FurnitureComponent &&) = default;

	// move assignment operator
	FurnitureComponent& operator=(FurnitureComponent &&) = default;

	// destructor
	virtual ~FurnitureComponent() = 0;

	virtual bool isDurable() const = 0;
};
inline FurnitureComponent::~FurnitureComponent()
{
	std::cout << "Base FurnitureComponent is destroyed" << std::endl;
}



#include "Furniture.h"
#include "Top.h"
#include "Leg.h"
#include "Metal.h"
#include "Wood.h"

int main()
{
	Furniture mixTable;

	auto const leg = std::make_shared<Leg>(Metal());
	auto const top = std::make_shared<Top>(Wood());

	mixTable.addComponent(leg);
	mixTable.addComponent(leg);
	mixTable.addComponent(leg);
	mixTable.addComponent(leg);
	mixTable.addComponent(top);

	std::cout << "Is the table durable?: " << std::boolalpha << mixTable.isDurable() << std::endl;

	return 0;
}

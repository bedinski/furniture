
#include "Furniture.h"
#include "FurnitureComponent.h"
#include <vector>

void Furniture::addComponent(std::shared_ptr<FurnitureComponent> const &component)
{
	components.push_back(component);
}

bool Furniture::isDurable() const
{
	if (!components.empty())
	{
		for (std::shared_ptr<FurnitureComponent> const &component : components)
		{
			if (!component->isDurable())
			{
				return false;
			}
		}
	}

	return true;
}
